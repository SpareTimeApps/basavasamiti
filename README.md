# READ ME #

### Overview ###

This repository is to maintain an application built for the Basava Samithi of New Zealand association. This is
a portfolio application for the association to reach out to the wider audience.  

### Technical details ###

The application is built using NodeJS and ReactJS.  It makes use of WebPack, react router, Bootstrap, SCSS,  Axios, react PDF js infinite, 
and relevant libraries that are typically used to build a NodeJS-ReactJS app. 