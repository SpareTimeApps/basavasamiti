import React from "react";

import App from "./js/components/app";
import About from "./js/components/about";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Contact from "./js/components/contactus";
import Members from "./js/components/members";
import Constitution from "./js/components/constitution";
import NavigationInstance from "./js/components/NavigationInstance";
ReactDOM.render(
  <wrapper>
    <BrowserRouter>
      <div>
        <NavigationInstance></NavigationInstance>
        <Switch>
          <Route path="/about" component={About} />
          <Route exact path="/gallery" component={About} />
          <Route exact path="/links" component={About} />
          <Route exact path="/contact" component={Contact} />
          <Route exact path="/members" component={Members} />
          <Route exact path="/constitution" component={Constitution} />
          <Route exact path="/" component={App} />}
        </Switch>
      </div>
    </BrowserRouter>
  </wrapper>,
  document.getElementById("root")
);
