import mickey from "../images/mickey.gif";

export default function committeeMembers() {
  return [
    {
      memberId: 1,
      first: "FirstName",
      last: "Surname",
      description: "'memberName' has been our esteemed committee memeber.",
      image: mickey,
    },
  ];
}
