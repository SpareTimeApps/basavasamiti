/**
 * Created by kavitha on 11/07/17.
 */
import React, { Component } from "react";

import { Link } from "react-router-dom";

class appHeader extends Component {
  render() {
    return (
      <div id="heroimage">
        <div className="appHeader">
          <div id="jumbotron">
            <h1 className="textcolor">Basava Samiti of New Zealand</h1>
            <h3 className="textcolor">Sharanu</h3>
          </div>
          <hr />
          <div className="wrapper">
            <Link to="/about" className="btn-lg btn-info ">
              Learn More{" "}
            </Link>
          </div>
        </div>
      </div>
    );
  }
}
export default appHeader;
