/**
 * Created by kavitha on 9/07/17.
 */
import React from "react";

import AppHeader from "./app-header";

const App = () => (
  <div>
    <AppHeader />
  </div>
);
export default App;
