import "bootstrap/dist/css/bootstrap.css";
import React, { Component } from "react";
import { LinkContainer } from "react-router-bootstrap";
import "../../scss/style.scss";
import { NavItem, Nav, Navbar, Container } from "react-bootstrap";

class NavigationInstance extends Component {
  render() {
    return (
      <div>
        <Navbar collapseOnSelect className="NavbarStyle">
          <Container>
            <Nav>
              <LinkContainer to="/">
                <NavItem eventKey={1}>Home</NavItem>
              </LinkContainer>
              <LinkContainer to="/about">
                <NavItem eventKey={2} href="/about">
                  About Us
                </NavItem>
              </LinkContainer>
              <LinkContainer to="/gallery">
                <NavItem eventKey={2} href="/gallery">
                  Gallery
                </NavItem>
              </LinkContainer>
              <LinkContainer to="/contact">
                <NavItem eventKey={3} href="/contact">
                  Contact Us
                </NavItem>
              </LinkContainer>
              <LinkContainer to="/links">
                <NavItem eventKey={4} href="/links">
                  Links
                </NavItem>
              </LinkContainer>
              <LinkContainer to="/members">
                <NavItem eventKey={5} href="/members">
                  Members
                </NavItem>
              </LinkContainer>
              <LinkContainer to="/constitution">
                <NavItem eventKey={6} href="/constitution">
                  Constitution
                </NavItem>
              </LinkContainer>
            </Nav>
          </Container>
        </Navbar>
      </div>
    );
  }
}

export default NavigationInstance;
