import React, { Component } from "react";
import members from "../reducers/committeemembers.js";
import {
  Button,
  FormLabel,
  Form,
  FormControl,
  FormGroup,
} from "react-bootstrap";

const wellStyles = { maxWidth: 400, margin: "0 auto 10px" };

class Contact extends Component {
  render() {
    var test = members();
    console.log();
    return (
      <div className="center">
        <div className="center_div">
          <h1>Drop us an email</h1>
          <Form method="POST" action="/contactsiteadmin">
            <FormGroup controlId="formControlsText">
              <FormControl>Name</FormControl>
              <FormControl
                componentClass="input"
                placeholder="field"
                name="name"
              />
            </FormGroup>
            <FormGroup controlId="formControlsText">
              <FormLabel>Email</FormLabel>
              <FormControl
                componentClass="input"
                placeholder="field"
                name="email"
              />
            </FormGroup>
            <FormGroup controlId="formControlsTextarea">
              <FormLabel>details</FormLabel>
              <FormControl
                componentClass="textarea"
                placeholder="textarea"
                name="message"
              />
            </FormGroup>
            <FormGroup controlId="formControlsSubmit">
              <Button
                bsStyle="primary"
                type="submit"
                className="form-control submit-btn"
              >
                Submit
              </Button>
            </FormGroup>
          </Form>
        </div>
      </div>
    );
  }
}

export default Contact;
