import React, { Component } from "react";
import myPdf from "../images/BSOAANZChapterConstitution.pdf";

import { PDFViewer } from "@react-pdf/renderer";
import MyDocument from "./pdf-document.js";

class Constitution extends Component {
  render() {
    return (
      <PDFViewer>
        <MyDocument />
      </PDFViewer>
    );
  }
}

export default Constitution;
